/*
 * Public Libs
*/
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QObject>
#include <QDebug>

/*
 * Private Libs
*/
#include "datamanager.h"
#include "audiothread.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine *engine = new QQmlApplicationEngine();
    engine->load(QUrl("qrc:/main.qml"));

    //AUDIO
    system("echo 36 > /sys/class/gpio/export");
    system("echo out > /sys/class/gpio/gpio36/direction");
    //OLD CIRCUIT
    system("echo 1 > /sys/class/gpio/gpio36/value");
    //NEW CIRCUIT
//    system("echo 0 > /sys/class/gpio/gpio36/value");
    system("amixer set 'Headphone' 100 % on");

    //BRIGHTNESS
    system("echo 1 > /sys/class/backlight/backlight/brightness");

    DataManager *dataManager;
    dataManager = new DataManager(engine);

    AudioThread *audioThread;
    audioThread = new AudioThread(engine);

    return app.exec();
}
