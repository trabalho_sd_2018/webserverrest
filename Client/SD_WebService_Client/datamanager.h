#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QQmlApplicationEngine>
#include <QObject>
#include <QQuickWindow>
#include <QQmlContext>
#include <QDebug>
#include <QQmlProperty>
#include <QtSerialPort/QSerialPort>
#include <QtNetwork>
#include <QNetworkAccessManager>
#include <QUrl>

class DataManager : public QObject
{
    Q_OBJECT

public:
    DataManager(QQmlApplicationEngine *engine);
    ~DataManager();

public slots:
    void httpPostData(QString urlPath, QString data);
    void httpGetData(QString urlPath);

private:
    QObject *rootContext;
    QQuickWindow *window;

    QSerialPort *rfidSerial;
    QString strSerialData;
    QByteArray serialData;
    QByteArray dataCksm;
    QByteArray toCheck;
    int cksm;

    QNetworkAccessManager *restClient;
    QNetworkReply *reply;
    QString urlHeader;
    QString postUrl;
    QString getUrl;

    void connectSerialPort();
    bool rfidChecksum(QByteArray data);

private slots:
    void readRfidSerial();
    void httpFinished();
};

#endif // DATAMANAGER_H
