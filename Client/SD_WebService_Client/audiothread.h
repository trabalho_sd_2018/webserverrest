#ifndef AUDIOTHREAD_H
#define AUDIOTHREAD_H

#include <QObject>
#include <QSoundEffect>
#include <QThread>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QQmlContext>

class AudioThread : public QThread
{
    Q_OBJECT

public:
    AudioThread(QQmlApplicationEngine *engine);

public slots:
    void playSoundEffect(QString source);

protected:
    void run();

signals:
    void playSignal();

private slots:
    void playSlot();
    void playAudio();

private:
    QObject *rootContext;
    QQuickWindow *window;

    QSoundEffect *effect;

    QStringList audioSourceList;
    QString audioSource;

    void play();
};

#endif // AUDIOTHREAD_H
