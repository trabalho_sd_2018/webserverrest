import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Extras 1.4

ApplicationWindow {
    visible: true
    width: 1280
    height: 800

    property bool status: false;

    SwipeView {
        id: swipeView
        objectName: "swipeView"
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1Form {
            StatusIndicator {
                id: statusInd
                objectName: "statusInd"
                anchors.centerIn: parent
                width: 100
                height: 100
                active: false
                color: status ? "green" : "red"

                function setAuthStatus(aStatus){
                    if(aStatus){
                        audioThread.playSoundEffect("qrc:/Sounds/SOUND_TRUE.wav");
                        txtStatus.text = qsTr("Entrada Autorizada!");
                        txtStatus.visible = true;
                        status = true;
                        statusInd.active = true;
                        statusTimer.start();
                    }else {
        //                audioThread.playSoundEffect("qrc:/Sounds/SOUND_FALSE.wav");
                        txtStatus.text = qsTr("Entrada Não Autorizada!");
                        txtStatus.visible = true;
                        status = false;
                        statusInd.active = true;
                        statusTimer.start();
                    }
                }
            }

            Text {
                id: txtStatus
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 260
                font.pixelSize: 36
                color: "#000"
                visible: false
            }

            Timer {
                id: statusTimer
                interval: 1500

                onTriggered: {
                    txtStatus.visible = false;
                    statusInd.active = false;
                }
            }
        }

        Page2Form {
            TextField {
                id: tfDelete
                objectName: "tfDelete"
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.top: parent.top
                anchors.topMargin: 20
                readOnly: true
            }

            Button {
                id: btnDelete
                width: 120
                anchors.left: tfDelete.right
                anchors.leftMargin: 20
                anchors.top: parent.top
                anchors.topMargin: 20
                text: "Deletar Tag"
                enabled: tfDelete.text !== "" ? true : false

                onClicked: {
                    dataManager.httpPostData("tag/delete", tfDelete.text);
                }
            }

            TextField {
                id: tfInsert
                objectName: "tfInsert"
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.top: parent.top
                anchors.topMargin: 80
                readOnly: true
            }

            Button {
                id: btnInsert
                width: 120
                anchors.left: tfInsert.right
                anchors.leftMargin: 20
                anchors.top: parent.top
                anchors.topMargin: 80
                text: "Inserir Tag"
                enabled: tfInsert.text !== "" ? true : false

                onClicked: {
                    dataManager.httpPostData("tag/insert", tfInsert.text);
                }
            }

            Button {
                id: btnClear
                width: 340
                height: 50
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.top: parent.top
                anchors.topMargin: 140
                text: "Limpar Campos"

                onClicked: {
                    tfDelete.text = "";
                    tfInsert.text = "";
                }
            }

            Timer {
                id: postTimer
                interval: 3000

                onTriggered: {
                    lbPostStatus.visible = false;
                    tfDelete.text = "";
                    tfInsert.text = "";
                }
            }

            Label {
                id: lbPostStatus
                objectName: "lbPostStatus"
                anchors.top: parent.top
                anchors.topMargin: 40
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 36
                font.weight: Font.Medium
                color: "#000"
                visible: false

                onVisibleChanged: {
                    if(visible)
                        postTimer.start();
                }
            }

            ListModel {
                id: lmLogList
            }

            Component {
                id: logDelegate
                Item {
                    width: 180
                    height: 40

                    Column {
                        Text { text: "Horário: " + datetime }
                        Text { text: "Tag: " + tag }
                    }
                }
            }

            ListView {
                id: logList
                objectName: "logList"
                anchors.top: parent.top
                anchors.topMargin: 260
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 20
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.right: parent.right
                model: lmLogList
                spacing: 20
                delegate: logDelegate

                function setLogData(strJson){
                    var logJson = JSON.parse(strJson).data;

                    for(var i = 0; i < logJson.length; i++)
                        lmLogList.append({"datetime": logJson[i].datetime, "tag": logJson[i].tag});;
                }
            }

            Button {
                id: btnGetInfo
                width: 340
                height: 50
                anchors.left: parent.left
                anchors.leftMargin: 20
                anchors.top: parent.top
                anchors.topMargin: 200
                text: "Mostrar Logs"

                onClicked: {
                    lmLogList.clear();
                    dataManager.httpGetData("log/getJson");
                }
            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Simulador")
        }
        TabButton {
            text: qsTr("Operações")
        }
    }
}
