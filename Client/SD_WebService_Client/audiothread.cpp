#include "audiothread.h"

AudioThread::AudioThread(QQmlApplicationEngine *engine)
{
    rootContext = engine->rootObjects().value(0);
    window = qobject_cast<QQuickWindow *>(rootContext);

    QQmlContext *ctx = engine->rootContext();
    ctx->setContextProperty("audioThread", this);

    effect = new QSoundEffect(this);
    QObject::connect(this, SIGNAL(playSignal()), this, SLOT(playSlot()));
    QObject::connect(effect, SIGNAL(loadedChanged()), this, SLOT(playAudio()));
}

void AudioThread::run()
{
    while(!audioSourceList.isEmpty()){
        play();
        audioSource = audioSourceList.first();
        audioSourceList.removeFirst();
    }
}

void AudioThread::playSlot()
{
    effect->setSource(QUrl(audioSource));

    if(effect->isLoaded())
        effect->play();
}

void AudioThread::playAudio()
{
    if(effect->isLoaded())
        effect->play();
}

void AudioThread::play()
{
    emit playSignal();
}

//PUBLIC SLOTS

void AudioThread::playSoundEffect(QString source)
{
    audioSourceList.append(source);

    if(!isRunning())
        start();
}
