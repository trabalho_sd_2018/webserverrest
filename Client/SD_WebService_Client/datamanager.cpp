#include "datamanager.h"

DataManager::~DataManager()
{
    if(rfidSerial->isOpen())
        rfidSerial->close();
}

DataManager::DataManager(QQmlApplicationEngine *engine)
{
    rootContext = engine->rootObjects().value(0);
    window = qobject_cast<QQuickWindow *>(rootContext);

    QQmlContext *ctx = engine->rootContext();
    ctx->setContextProperty("dataManager", this);

    urlHeader = "http://192.168.0.126:8080/Server/webresources/data/";

    restClient = new QNetworkAccessManager(this);

    rfidSerial = new QSerialPort();
    connectSerialPort();
}

//RFID METHODS

void DataManager::connectSerialPort()
{
    rfidSerial->setPortName("/dev/ttymxc3");
    rfidSerial->open(QSerialPort::ReadWrite);
    rfidSerial->setBaudRate(QSerialPort::Baud19200, QSerialPort::AllDirections);
    rfidSerial->setParity(QSerialPort::NoParity);
    rfidSerial->setStopBits(QSerialPort::OneStop);
    rfidSerial->setFlowControl(QSerialPort::NoFlowControl);

    if(rfidSerial->isOpen()){
        qDebug() << "dataManager RFID SERIAL PORT CONNECTED";
        rfidSerial->clear(QSerialPort::AllDirections);
        QObject::connect(rfidSerial, SIGNAL(readyRead()), this, SLOT(readRfidSerial()));
    }
}

void DataManager::readRfidSerial()
{
    serialData = rfidSerial->readLine();

    if(rfidChecksum(serialData)){
        strSerialData = serialData.mid(4, 4).toHex();

        if(QQmlProperty::read(window->findChild<QObject *>("swipeView"), "currentIndex").toInt() == 0){
            httpGetData("tag/getTagValid/" + strSerialData);
        }else if(QQmlProperty::read(window->findChild<QObject *>("swipeView"), "currentIndex").toInt() == 1){
            QQmlProperty::write(window->findChild<QObject *>("tfDelete"), "text", strSerialData);
            QQmlProperty::write(window->findChild<QObject *>("tfInsert"), "text", strSerialData);
        }
    }
}

bool DataManager::rfidChecksum(QByteArray data)
{
    if(data.size() < 9 || data.size() > 9)
        return false;

    cksm = 0;
    dataCksm = data.mid(8, 1).toHex();
    toCheck = data.mid(2, 6);

    foreach (QChar chr, toCheck)
        cksm = cksm ^ chr.toLatin1();

    if(cksm == dataCksm.toInt(nullptr, 16))
        return true;

    return false;
}

//HTTP METHODS

void DataManager::httpPostData(QString urlPath, QString data)
{
    postUrl = urlPath;
    QUrl url(urlHeader + urlPath);
    QNetworkRequest request(url);
    request.setRawHeader("Content-Type", "text/plain");

    QByteArray postData;
    postData.append(data);

    reply = restClient->post(request, postData);
    QObject::connect(reply, SIGNAL(finished()), this, SLOT(httpFinished()));
}

void DataManager::httpGetData(QString urlPath)
{
    getUrl = urlPath;
    QNetworkRequest request;
    request.setUrl(QUrl(urlHeader + urlPath));

    reply = restClient->get(request);
    QObject::connect(reply, SIGNAL(finished()), this, SLOT(httpFinished()));
}

void DataManager::httpFinished()
{
    if(QQmlProperty::read(window->findChild<QObject *>("swipeView"), "currentIndex").toInt() == 0){
        if(reply->readAll() == "true")
            QMetaObject::invokeMethod(window->findChild<QObject *>("statusInd"), "setAuthStatus", Q_ARG(QVariant, true));
        else
            QMetaObject::invokeMethod(window->findChild<QObject *>("statusInd"), "setAuthStatus", Q_ARG(QVariant, false));
    }else if(QQmlProperty::read(window->findChild<QObject *>("swipeView"), "currentIndex").toInt() == 1){
        if(postUrl == "tag/insert"){
            if(reply->readAll() == "true"){
                QQmlProperty::write(window->findChild<QObject *>("lbPostStatus"), "text", "Tag cadastrada!");
                QQmlProperty::write(window->findChild<QObject *>("lbPostStatus"), "visible", true);
            }else {
                QQmlProperty::write(window->findChild<QObject *>("lbPostStatus"), "text", "Tag já cadastrada!");
                QQmlProperty::write(window->findChild<QObject *>("lbPostStatus"), "visible", true);
            }
        }else if(postUrl == "tag/delete"){
            if(reply->readAll() == "true"){

            }else {

            }
        }else if(getUrl == "log/getJson"){
            QMetaObject::invokeMethod(window->findChild<QObject *>("logList"), "setLogData", Q_ARG(QVariant, reply->readAll()));
        }
    }

    postUrl = "";
    getUrl = "";
}

