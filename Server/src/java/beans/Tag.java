package beans;

public class Tag {
    
    private String rfid;

    public Tag() {
    }

    public Tag(String rfid) {
        this.rfid = rfid;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

}
