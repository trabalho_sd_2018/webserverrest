package beans;

import java.util.Calendar;

public class Log {
    
    private Calendar horario;
    private String rfid;

    public Log() {
    }

    public Log(Calendar horario, String rfid) {
        this.horario = horario;
        this.rfid = rfid;
    }

    public Calendar getHorario() {
        return horario;
    }

    public void setHorario(Calendar horario) {
        this.horario = horario;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }
    
}
