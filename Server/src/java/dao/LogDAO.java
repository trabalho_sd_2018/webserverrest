package dao;

import beans.Log;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.ConectaBD;

public class LogDAO {
    
    private static Timestamp ts;

    public static void insert(Log obj) {
        PreparedStatement pst;
        try {
            pst = ConectaBD.getConection().prepareStatement(
                    "INSERT INTO log (horario, rfid) VALUES (?, ?)");
            
            ts = new Timestamp(obj.getHorario().getTimeInMillis());

            pst.setTimestamp(1, ts);
            pst.setString(2, obj.getRfid());
            pst.execute();
        } catch (SQLException ex) {
            Logger.getLogger(TagDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static String findAll() {
        List<Log> logs = new ArrayList<>();
        ResultSet rs;
        PreparedStatement pst;

        try {
            pst = ConectaBD.getConection().prepareStatement(
                    "SELECT * FROM log ORDER BY horario");
            rs = pst.executeQuery();
            while(rs.next()) {
                ts = rs.getTimestamp("horario");
                Calendar c = Calendar.getInstance();
                c.setTime(ts);
                
                logs.add(new Log(c, rs.getString("rfid")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(LogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // MONTA JSON
        final DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        String strLogs = "{ \"data\": [ ";
        for (Log l : logs) {
            strLogs += "{ \"datetime\": \"" + df.format(l.getHorario().getTime()) + 
                     "\", \"tag\": \"" + l.getRfid() + "\" },";
        }
        strLogs = strLogs.substring(0, strLogs.length() - 1); // remove virgula
        strLogs += " ] }";
        
        return strLogs;
    }
    
}