package dao;

import beans.Tag;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import util.ConectaBD;

public class TagDAO {

    public static Boolean insert(Tag obj) {
        PreparedStatement pst;
        try {
            pst = ConectaBD.getConection().prepareStatement(
                    "INSERT INTO tag (rfid) VALUES (?)");
            pst.setString(1, obj.getRfid());
            pst.execute();
            return true;
        } catch (SQLException ex) {
            System.out.println("Erro insert");
            return false;
        }
    }

    public static void delete(Tag obj) {
        PreparedStatement pst;
        try {
            pst = ConectaBD.getConection().prepareStatement(
                    "DELETE FROM tag WHERE rfid = ?");
            pst.setString(1, obj.getRfid());
            pst.execute();
        } catch (SQLException ex) {
            System.out.println("Erro delete");
        }
    }

    public static boolean findRfid(String rfid) {
        ResultSet rs;
        PreparedStatement pst;
        
        try {
            pst = ConectaBD.getConection().prepareStatement(
                    "SELECT * FROM tag WHERE rfid = ?");
            pst.setString(1, rfid);
            rs = pst.executeQuery();
            
            return rs.next();
            
        } catch (SQLException ex) {
            System.out.println("Erro findRfid");
            return false;
        }
    }

}
