package ws;

import beans.Log;
import beans.Tag;
import dao.LogDAO;
import dao.TagDAO;
import java.util.Calendar;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

@Path("data")
public class RestWS {

    @Context
    private UriInfo context;

    public RestWS() {
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getInput() {
        return "";
    }
    
    /* TAG */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("tag/insert")
    public String insertTag(String rfid) {
        Tag obj = new Tag(rfid);
        return TagDAO.insert(obj).toString();
    }

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Path("tag/delete")
    public void deleteTag(String rfid) {
        Tag obj = new Tag(rfid);
        TagDAO.delete(obj);
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("tag/getTagValid/{rfid}")
    public String getTagValid(@PathParam("rfid") String rfid) {        
        if (TagDAO.findRfid(rfid)) {        
            Calendar c = Calendar.getInstance();
            Log l = new Log(c, rfid);
            LogDAO.insert(l);
            
            return "true";
        } else { 
            return "false";
        }
    }
    
    /* LOG */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("log/getJson")
    public String getListLogs() {
        return LogDAO.findAll();
    }

}